package com.example.alkapractical;

import android.app.Application;
import com.squareup.picasso.BuildConfig;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import okhttp3.OkHttpClient;

/**
 * Created by alka_kashyap on 2019-08-19.
 */
public class MyApplication extends Application {
    private Picasso picasso;

    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient okHttpClient = new OkHttpClient();

        picasso = new Picasso.Builder(this)
                .indicatorsEnabled(BuildConfig.DEBUG)
                .loggingEnabled(BuildConfig.DEBUG)
                .downloader(new OkHttp3Downloader(new OkHttpClient()))
                .build();
    }

    Picasso providePicasso() {
        return picasso;
    }
}
