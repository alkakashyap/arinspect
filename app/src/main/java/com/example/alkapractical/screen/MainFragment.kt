package com.example.alkapractical.screen

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.alkapractical.adapter.MyAdapter
import com.example.alkapractical.interfaces.OnListItemClickListener
import com.example.alkapractical.model.Row
import com.example.alkapractical.model.ListResponse
import com.example.alkapractical.rest.APIClient
import kotlinx.android.synthetic.main.fragment_main_list.*
import retrofit2.Callback
import retrofit2.Response

import java.util.ArrayList
import retrofit2.Call
import androidx.recyclerview.widget.DividerItemDecoration
import android.graphics.drawable.ClipDrawable.VERTICAL


class MainFragment : androidx.fragment.app.Fragment() {

    var list: MutableList<Row> = ArrayList()
    var adapter: MyAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(com.example.alkapractical.R.layout.fragment_main_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val layoutManager = LinearLayoutManager(context)
        recycler_view.setHasFixedSize(true)
        recycler_view.layoutManager = layoutManager
        val decoration = DividerItemDecoration(context, LinearLayoutManager.VERTICAL)
        recycler_view.addItemDecoration(decoration)

        adapter = context?.let { MyAdapter(it, list, object: OnListItemClickListener{
            override fun onListItemClick(item: Row) {
            }
        })
        }
        recycler_view.adapter = adapter
        getListData()
    }

    private fun getListData() {

        val call = APIClient.getAPIService().dataList
        call.enqueue(object : Callback<ListResponse> {
            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                list.addAll(response.body()?.rows as MutableList<Row>)
                recycler_view?.adapter?.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                call.cancel()
            }
        })
    }

    override fun onDetach() {
        super.onDetach()
    }
}
