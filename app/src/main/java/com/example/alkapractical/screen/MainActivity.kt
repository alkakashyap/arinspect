package com.example.alkapractical.screen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.alkapractical.R

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragment()
    }

    private fun loadFragment() {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frame, MainFragment()).commit()
    }
}
