package com.example.alkapractical.interfaces

import com.example.alkapractical.model.Row

/**
 * Created by alka_kashyap on 2019-08-19.
 */
interface OnListItemClickListener {
    fun onListItemClick(item : Row)
}
