package com.example.alkapractical.model

data class ListResponse(
    val rows: List<Row>,
    val title: String
)