package com.example.alkapractical.rest;

import com.example.alkapractical.model.ListResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface ApiInterface {

    @GET("s/2iodh4vg0eortkl/facts.json")
    Call<ListResponse> getDataList();
}
