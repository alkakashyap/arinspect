package com.example.alkapractical.adapter

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.alkapractical.R
import com.example.alkapractical.interfaces.OnListItemClickListener

import com.example.alkapractical.model.Row
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.list_item.view.*
import java.lang.Exception

class MyAdapter(
    private val mContext: Context,
    private val mValues: List<Row>,
    private val mListener: OnListItemClickListener?
) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Row
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListItemClick(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mTitleView.text = item.title
        holder.mDescriptionView.text = item.description

        Picasso.get().load(item.imageHref).placeholder(R.drawable.ic_launcher_background).error(
            R.drawable.ic_launcher_background
        ).resize(100,100).into(holder.mImageView, object : Callback {
            override fun onSuccess() {
                Log.e("onSuccess", item.imageHref)
            }

            override fun onError(e: Exception?) {
                Log.e("onError$e", item.imageHref)
            }
        })

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mTitleView: TextView = mView.title
        val mImageView: ImageView = mView.image
        val mDescriptionView: TextView = mView.description

        override fun toString(): String {
            return super.toString() + " '" + mDescriptionView.text + "'"
        }
    }
}
